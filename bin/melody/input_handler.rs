#[derive(Copy, Clone, Debug)]
pub enum Action {
    // ScrollUp,
    // ScrollDown,
    Skip,
    PlayPause,
    Rewind,
    // Stop,
    Quit,
    VolumeUp,
    BiggerVolumeUp,
    VolumeDown,
    BiggerVolumeDown,
    Shuffle,
}

pub fn read_async(reader: &mut ::crossterm::AsyncReader) -> Option<Action> {
    if let Some(::crossterm::InputEvent::Keyboard(key_event)) = reader.next() {
        match key_event {
            ::crossterm::KeyEvent::Char(c) => match c {
                'q' => Some(Action::Quit),
                'n' => Some(Action::Skip),
                'b' => Some(Action::Rewind),
                '>' => Some(Action::VolumeUp),
                '<' => Some(Action::VolumeDown),
                'p' => Some(Action::PlayPause),
                's' => Some(Action::Shuffle),
                _ => None,
            },
            ::crossterm::KeyEvent::Ctrl(c) => match c {
                'c' => Some(Action::Quit),
                '>' => Some(Action::BiggerVolumeUp),
                '<' => Some(Action::BiggerVolumeDown),
                _ => None,
            },
            _ => None,
        }
    } else {
        None
    }
}
