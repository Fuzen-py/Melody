use std::error::Error as StdError;
use std::fmt;
use std::io::{Error as IoError, ErrorKind as IoErrorKind};
use std::path::{Path, PathBuf};

#[derive(Debug)]
/// Errors from Melody
pub struct MelodyErrors {
    /// Kind of error
    pub(crate) kind: MelodyErrorsKind,
    /// Description of error
    pub(crate) description: String,
    /// Path to file related to error
    pub(crate) file: Option<PathBuf>,
}

impl StdError for MelodyErrors {
    fn description(&self) -> &str {
        &self.description
    }
}
impl fmt::Display for MelodyErrors {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(self.description())
    }
}
/// MelodyErrors
/// # Example
/// ```
/// match ::std::env::current_dir() {
///     Ok(path) => {
///         if !path.exists() {
///             Err(melody::MelodyErrors::new(melody::MelodyErrorsKind::PathDoesNotExist, "Path does not exist", Some(&path)))
///         } else {
///             Ok(path)
///         }
///     },
///     Err(e) => Err(e.into())
/// };
/// ```
impl MelodyErrors {
    /// Creates a melody error
    /// kind: Type of error
    /// description: Description of the rror
    /// File: Optionally the file that raised this error
    pub fn new(kind: MelodyErrorsKind, description: &str, file: Option<&Path>) -> Self {
        Self {
            kind,
            description: description.to_string(),
            file: match file {
                Some(file) => Some(file.to_path_buf()),
                None => None,
            },
        }
    }
    /// Return the error kind
    pub fn kind(&self) -> MelodyErrorsKind {
        self.kind
    }
    /// Return the file associated with the error
    pub fn file(&self) -> Option<PathBuf> {
        self.file.clone()
    }
}

impl From<IoError> for MelodyErrors {
    fn from(err: IoError) -> Self {
        MelodyErrors::new(MelodyErrorsKind::Io(err.kind()), err.description(), None)
    }
}

// impl From<Mp3MetadataError> for MelodyErrors {
//     fn from(err: Mp3MetadataError) -> Self {
// 	       MelodyErrors {
//		       kind: MelodyErrorsKind::Mp3MetadataError(err),
// 			   description: err.description().to_string(),
// 			   file: None
// 		  }
//     }
// }

/// Kind of errror that arose from Melody
#[derive(Clone, Copy, Debug)]
pub enum MelodyErrorsKind {
    /// IO error
    Io(IoErrorKind),
    /// The path was not absolute
    NotAbsolutePath,
    /// Path does not exist
    PathDoesNotExist,
    /// The path was expected to be a dir
    PathIsNotADir,
    /// The path was expected to be a file
    PathIsNotAFile,
    /// Recursion error caused by the selecting the child as the move of a parent
    ChildOfParentRecursion,
    /// Failed to get the directory above selected dir
    FailedToFindParent,
    /// File type is not supported
    UnsupportedFileType,
    /// Failed to the read the tag of the file
    FailedToReadTag,
    /// Unknown file type, not supported
    UnkownFileType,
    /// Failed to get the file's extention
    CanNotReadFileEXT,
    /// The queue is empty, cannot play from an empty queue
    EmptyQueue,
    /// Music player is not paused
    NotPaused,
    /// Music player is already playing, cannot resume
    AlreadyPlaying,
    /// Failed to get song duration
    MissingDuration,
    /// Failed to read the metadata of the song
    MetaDataError,
}
