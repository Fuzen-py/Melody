Melody
======
Terminal music player written in rust

**Doesn't work on WSL**

**Note:** Some songs wont be loaded because [I cant get the duration](https://gitlab.com/Fuzen-py/Melody/issues/1).


### Requirements

- [Rust](https://rustup.rs)


### Linux Specific:

- alsa development headers

## Environment Variables

- `MELODY_VOLUME` - Sets volume EX: `MELODY_VOLUME=0.25`
- `MELODY_MUSIC` - Sets the music directory EX `MELODY_MUSIC=$HOME/Music`
- `MELODY_PRIORITIZE_CWD` - Prioritizes the CWD over `MELODY_MUSIC`
- `MELODY_IGNORE_ALL_UNKNOWNS` - Ignores songs with unknown artists/album/title
- `MELODY_IGNORE_UNKNOWN_TITLE` - Ignores songs with an unknown title
- `MELODY_IGNORE_UNKNOWN_ALBUM` - Ignores songs with an unknown album
- `MELODY_IGNORE_UNKNOWN_ARTIST` - Ignores songs with an unknown artist

## Config
The config's location should follow your OS's standard. The location will be printed out at runtime.

- volume - Set's the default volume 0.25 by default
- music - set's the default path to look for, this is your OS's standard music dir by default
- prioritize_cwd -  set's prioritizing the current dir as a priority, false by default
- ignore_unknown_title - Ignores songs with an unknown title, false by default
- ignore_unknown_album - Ignores songs with an unknown album, false by default
- ignore_unknown_artist - Ignores songs with an unknown artist, false by default
- ignore_all_unknowns - Ignores songs with all or any unknown field (artist,album,title)

### CLI Usage

    USAGE:
        melody [FLAGS] [OPTIONS]

    FLAGS:
        -h, --help                     Prints help information
        -m, --minimal                  Runs Melody in minimal mode. Disables Queue preview and shuffle.
            --ignore-unknown           Ignore all unknowns
            --ignore-unknown-album     Ignores Unknown albums
            --ignore-unknown-artist    Ignores unknown artists
            --ignore-unknown-title     Ignores Unknown title
        -V, --version                  Prints version information

    OPTIONS:
        -p, --path <path>        Music directory you wish to listen from.
        -v, --volume <volume>    Sets volume 0.5 = 50%
